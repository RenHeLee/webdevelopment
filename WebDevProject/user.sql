CREATE TABLE  `user` (
`user_id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
`username` VARCHAR( 50 ) NOT NULL, 
`hashedpassword` VARCHAR( 250 ) NOT NULL
) ;

INSERT INTO user (username, hashedpassword) VALUE('admin', '03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4');

CREATE TABLE  `contact` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY,
`contact_name` VARCHAR( 50 ) NOT NULL,
`email` VARCHAR( 250 ) NOT NULL, 
`phone` VARCHAR( 15 ) NOT NULL
) ;

INSERT INTO user (contact_name, email, phone) VALUES('Lee Ren He', 'ai170190@siswa.uthm.edu.my', '017-7332482'), ('Siti Aisyah Binti Mohd Khamsani', 'Siti Aisyah Binti Mohd Khamsani', '011-57758684'), ('Sarah Yumni Binti Suwandi', 'yummmnny@gmail.com', '013-9734879'), ('Siti Khadijah Binti Mahamad Noor', 'khadijahsiti366@gmail.com', '012-2646280'), ('Siti Khadijah Binti Talib', 'sitikhadijahtalib98@gmail.com', '019-7837591');

CREATE TABLE  `feedback` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
`enquiryname` VARCHAR( 50 ) NOT NULL, 
`email` VARCHAR( 250 ) NOT NULL, 
`title` VARCHAR( 100 ) NOT NULL, 
`branch` VARCHAR( 50 ) NOT NULL,
`feedback` VARCHAR( 3000 ) NOT NULL,
`feedbacktime` DATETIME NOT NULL
) ;

CREATE TABLE  `membership` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
`membershipname` VARCHAR( 50 ) NOT NULL, 
`email` VARCHAR( 250 ) NOT NULL, 
`ic` VARCHAR( 14 ) NOT NULL, 
`branch` VARCHAR( 50 ) NOT NULL,
`address` VARCHAR( 300 ) NOT NULL,
`applytime` DATETIME NOT NULL
) ;