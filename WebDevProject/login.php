<!-- Login Page -->
<?php
// Start Session
session_start();

require 'database.php';
$login_error_message = '';

// check Login request
if (! empty($_POST['loginForm'])) {

    $name = trim($_POST['name']);
    $password = trim($_POST['password']);

    $user_id = Admin::login($name, $password); // check user login
    if ($user_id > 0) {
        $_SESSION['user_id'] = $user_id; // Set Session
        header("Location: membershipManagement.php"); // Redirect user to the profile.php
    } else {
        $login_error_message = 'Invalid login details!';
    }
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style2.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<title>Login</title>
</head>
<body>
	<div class="topnav">
		<a class="active" href="index.php">Home</a> <a href="Membership.php">Membership</a>
		<a href="Promotion.php">Promotion</a> <a href="Feedback.php">Feedback</a>
		<a href="About.php">About</a>
	</div>
	<div class="header">
		<img src="logo.png" alt="logo" width="371" height="257">
		<h2>S I N C E &nbsp 1 9 9 3</h2>
	</div>
	<div style="height: 40%; text-align: center;">
		<h2>Login</h2>
				<?php
    if ($login_error_message != "") {
        echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $login_error_message . '</div>';
    }
    ?>
					<form action="login.php" method="post">
			<p>Name: </p><input type="text" id="name" name="name"><br>
			<p>Password: </p><input type="password" id="password" name="password"><br>
			<input type="submit" name="loginForm" value="Login"> <input type="reset" value="Reset"><br>
		</form>
	</div>
	<div class="footer"><?php include 'footer.php';?></div>
	</div>
</body>
</html>