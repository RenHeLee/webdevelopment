<!DOCTYPE HTML>
<html>
<head>
<title>ABOUT</title>
<link rel="stylesheet" href="style2.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>

	<div class="topnav">
		<a href="index.php">Home</a> <a href="Membership.php">Membership</a>
		<a href="Promotion.php">Promotion</a> <a href="Feedback.php">Feedback</a>
		<a class="active" href="About.php">About</a>
	</div>
	<div class="header">
		<img src="logo.png" alt="logo" width="371" height="257">
		<h2>S I N C E &nbsp 1 9 9 3</h2>
	</div>

	<div style="padding-left: 7%; padding-right: 7%">
		<h2>Background of Hussein Onn Mart</h2>
		<p>The supermarket was formerly known as Kedai Runcit Parit Raja which was established on September 16, 1993 at Arked. The core business of Kedai Runcit Parit Raja then was to solve the problem of buying necessities by the student.</p>
		<p>Three years later, Kedai Runcit Parit Raja was upgraded to Hussein Onn Mini Market. On September 27, 2000, the institute achieved another milestone when the Hussein Onn Mini Market upgraded to Hussein Onn Mart. This status served to recognize its contributions in the area of Universiti Tun Hussein Onn.</p>
		<p>Hussein Onn Mart continued to progress and achieved another milestone on September 20, 2006, which decided to have branches in Arked. On February 1, 2007, another branch of Hussein Onn Mart is launched at Perwira. Subsequently, branch of Parit Raja Supermarket is launched at Bestari College on March 2, 2007.</p>
		<br> 
		<iframe width="560" height="315" src="https://www.youtube.com/embed/l2MnwvcajuM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		<br><br>


		<h2>Location of Hussein Onn Mart</h2>
		<p>
			<b> HQ Address :</b>HUSSEIN ONN MART <br> 86400 Parit Raja <br> Batu
			Pahat Johor , Malaysia
		</p>
		<img
			src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_VWmnNI3tWsUvJDGyKgXNyjjF4M6ka9UeW_xRWKi1UdULB_wabQ"
			alt="Peta UTHM" width="70%">

		<h2>Contact Us</h2>
		<table style="border: medium; border-color: black;">
			<tr>
				<td>Name</td>
				<td>Email</td>
				<td>Contact Number</td>
			</tr>
			<?php
    require 'database.php';
    $contact = client::selectContact();

    $str = '';
    if (client::isMultidimensional($contact)) {
        foreach ($contact as $key) {
            $str .= "<tr><td>$key[contact_name]</td><td><a href=\"mailto:$key[email]\">$key[email]</a></td><td>$key[phone]<br><img src=\"https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=$key[phone]\"></td></tr><tr></tr>";
        }
        ;
    } else {
        $str .= "<tr><td>$contact[contact_name]</td><td><a href=\"mailto:$contact[email]\">$contact[email]</a></td><td>$contact[phone]<br><img src=\"https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=$contact[phone]\"</td></tr><tr></tr>";
    }
    echo $str;
    ?>
		</table>
	</div>
	<div class="footer"><?php include 'footer.php';?>
</div>
</body>
</html>

