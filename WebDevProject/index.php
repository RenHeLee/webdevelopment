<!DOCTYPE html>
<html>
<head>
<title>HOME PAGE</title>
<link rel="stylesheet" href="style2.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
	<div class="topnav">
		<a class="active" href="index.php">Home</a> <a href="Membership.php">Membership</a>
		<a href="Promotion.PHP">Promotion</a> <a href="Feedback.php">Feedback</a>
		<a href="About.php">About</a>
	</div>

	<div class="header">
		<h1>HUSSEIN ONN MART</h1>
		<img src="logo.png" alt="logo" width="371" height="257">
		<h2>S I N C E &nbsp 1 9 9 3</h2>

	</div>

	<center>
		<div class="slideshow-container">

			<div class="mySlides fade">
				<div class="numbertext">1 / 3</div>
				<img src="1.png" style="width: 30px height:50">
				<div class="text">page 1</div>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">2 / 3</div>
				<img src="2.png" style="width: 30px height:50">
				<div class="text">Page 2</div>
			</div>

			<div class="mySlides fade">
				<div class="numbertext">3 / 3</div>
				<img src="3.png" style="width: 30px height:50">
				<div class="text">page 3</div>
			</div>

		</div>
		<br>

		<div style="text-align: center">
			<span class="dot"></span> <span class="dot"></span> <span class="dot"></span>
		</div>

		<script>
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 2000); // Change image every 2 seconds
}
</script>
	</center>

	<div class="footer"><?php include 'footer.php';?></div>
</body>
</html>