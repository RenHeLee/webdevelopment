<!-- Management page of contact -->
<?php
// Start Session
session_start();

// check user login
if (empty($_SESSION['user_id'])) {
    header("Location: login.php");
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<link rel="stylesheet" href="style2.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<script type="text/javascript">


function insertContact(){
	var contact_name = document.getElementById("contact_name").value;
	var email = document.getElementById("email").value;
	var phone = document.getElementById("phone").value;
	var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	      document.getElementById("list").innerHTML =
	      this.responseText;
	    }
	  };
	  xhttp.open("GET", "contactList.php?id=&contact_name=" + contact_name + "&email=" + email + "&phone=" + phone, true);
	  xhttp.send(); 
	loadInsert();
}

function deletecontact(id){
		var xhttp = new XMLHttpRequest();
		  xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		      document.getElementById("list").innerHTML =
		      this.responseText;
		    }
		  };
		  xhttp.open("GET", "contactList.php?id=" + id + "&contact_name=&email=&phone=", true);
		  xhttp.send(); 
}


function loadList() {
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	      document.getElementById("list").innerHTML =
	      this.responseText;
	    }
	  };
	  xhttp.open("GET", "contactList.php?id=&contact_name=&email=&phone=", true);
	  xhttp.send(); 
}

function loadInsert() {
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	      document.getElementById("form").innerHTML =
	      this.responseText;
	    }
	  };
	  xhttp.open("GET", "contactInsert.php", true);
	  xhttp.send(); 
}

loadList();
loadInsert();
</script>
<title>Contact management</title>
</head>
<body>
	<div class="topnav">
		<a href="index.php">Home</a> <a
			href="membershipManagement.php">Membership Management</a> <a
			href="feedbackManagement.php">Feedback Management</a> <a
			class="active" href="contactManagement.php">Contact Management</a>
	</div>
	<div class="content">
		<div id="list" name="list"></div>
		<div id="form" name="form"></div>
	</div>
	<div class="footer"><?php include 'footer.php';?></div>
</body>
</html>