<?php
/**
 * logout.php
 *
 * allow user logout and reditect to index.php
 *
 * @version    1.1 2018-10-20
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// start session
session_start();

// Destroy user session
unset($_SESSION['user_id']);

// Redirect to index.php page
header("Location: index.php");
?>