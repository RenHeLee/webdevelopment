<!-- Enquiry page for membership -->

<?php
$enquiry_message = '';
if (! empty($_POST['MembershipForm'])) {
    require 'database.php';
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $ic = trim($_POST['ic']);
    $phoneNo = trim($_POST['phoneNo']);
    $branch = trim($_POST['branch']);
    $address = trim($_POST['address']);

    client::insertMembership($name, $email, $phoneNo, $ic, $branch, $address);
    $enquiry_message = 'Membership request is sent successfully.';
}

?>
<!DOCTYPE html>
<html>
<head>
<title>MEMBERSHIP</title>
<link rel="stylesheet" href="style2.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
	<div class="topnav">
		<a href="index.php">Home</a> <a class="active" href="Membership.php">Membership</a>
		<a href="Promotion.php">Promotion</a> <a href="Feedback.php">Feedback</a>
		<a href="About.php">About</a>

	</div>
	<div class="header">

		<img src="logo.png" alt="logo" width="371" height="257">
		<h2>S I N C E &nbsp 1 9 9 3</h2>
	</div>

	<div style="padding-left: 7%; padding-right: 7%">
			<div style="text-align: center;">
			<?php
if ($enquiry_message != "") {
    echo '<strong>Message: </strong> ' . $enquiry_message;
}
?>
			</div>
		<h2 style="text-align: center;">
			<i class="material-icons" style="font-size: 60px; color: lightblue;">cloud</i>
			The Hussein Card<i class="material-icons"
				style="font-size: 60px; color: lightblue;">cloud</i>
		</h2>

		<p style="text-align: center;">Enjoy even more shopping privileges</p>
		<br> <br>

		<ul>
			<h3>
				<li>FREE Personal Accident Insurance
			
			</h3>
			</li>
			<p>
				<i class="material-icons">attachment</i>You will be automatically
				protected with FREE Personal Accident Insurance up to RM10,000 for
				death and permanent disablement benefits which <br>provides 24 hours
				coverage worldwide.
			</p>

			<h3>
				<li>Nationwide Reward Points Accumulation
			
			</h3>
			</li>
			<p>
				<i class="material-icons">attachment</i>Simply present yours The
				Store Card while you shop at The Store outlets, collect 1 reward
				point for every RM1.00 spend.
			</p>

			<h3>
				<li>Attractive Reward Points Redemption Program
			
			</h3>
			</li>
			<p>
				<i class="material-icons">attachment</i> Redeem attractive gifts
				with your points through our Reward Points Redemption Program.
			</p>

			<h3>
				<li>Exclusive Invitations
			
			</h3>
			</li>
			<p>
				<i class="material-icons">attachment</i> Get an exclusive invitation
				to enjoy more discounts, benefits, promotions and activities during
				our special events.
			</p>

			<h3>
				<li>Special Member's Price
			
			</h3>
			</li>
			<p>
				<i class="material-icons">attachment</i> Look out for Special
				Member's Price items to help you save more
			</p>
			<br>
			<br>
			<div style="padding-left: 10%; padding-right: 10%;">
	<form action="Membership.php" method="post">
					<h2 align="center">A P P L I C A T I O N &nbsp F O R &nbsp M E M B
						E R S H I P</h2>
					<div align="center">
						<p>Name:</p>
						<input type="text" id="name" name="name" style="width: 50%"
							required><br>
						<p>Email:</p>
						<input type="email" id="email" name="email" style="width: 50%"
							required><br>
						<p>I/C Number:</p>
						<input type="text" id="ic" name="ic" style="width: 50%" required><br>
						<p>Phone Number:</p>
						<input type="text" id="phoneNo" name="phoneNo" required
							style="width: 50%"><br>
						<p>Branch:</p>
						<select id="branch" name="branch" required style="width: 50%">
							<option value="">Please select branch</option>
							<option value="Arked">Arked</option>
							<option value="Bestari">Bestari</option>
							<option value="Perwira">Perwira</option>
							<br>
						</select><br>
						<p>Address:</p>
						<textarea rows="8" cols="100" id="address" name="address"
							style="width: 50%"></textarea>
						<br>
						<input type="submit" value="Submit" name="MembershipForm"> <input
							type="reset" value="Reset"><br>
					</div>
				</form>
			</div>
	
	</div>
	<div class="footer"><?php include 'footer.php';?>
</div>
</body>
</html>