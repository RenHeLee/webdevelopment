<!-- Management page for membership request -->
<?php
// Start Session
session_start();

// check user login
if (empty($_SESSION['user_id'])) {
    header("Location: login.php");
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style2.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<title>Membership Management</title>
</head>
<body>
	<div class="topnav">
		<a href="index.php">Home</a> <a
			class="active" href="membershipManagement.php">Membership Management</a> <a
			href="feedbackManagement.php">Feedback Management</a> <a
			href="contactManagement.php">Contact Management</a>
	</div>
	<div class="content">
		<h2>Membership request</h2>
			<?php
require 'database.php';
$membership = Admin::selectMembership();

$str = '';
if (Admin::isMultidimensional($membership)) {
    foreach ($membership as $key) {
        $str .= "<table>
                        <tr>
                            <td>Name: </td>
                            <td>$key[membershipname]</td>
                        </tr>
                        <tr>
                            <td>IC: </td>
                            <td>$key[ic]</td>
                        </tr>
                        <tr>
                            <td>Phone: </td>
                            <td>$key[phone]</td>
                        </tr>
                        <tr>
                            <td>Address: </td>
                            <td>$key[address]</td>
                        </tr>
                        <tr>
                            <td>Email: </td>
                            <td>$key[email]</td>
                        </tr>
                        <tr>
                            <td>Branch: </td>
                            <td>$key[branch]</td>
                        </tr>
                        
                        <tr>
                            <td>Time: </td>
                            <td>$key[applytime]</td>
                        </tr>
				</table><br>";
    }
    ;
} else {
    $str .= "<table>
                <tr>
                    <td>Name: </td>
                    <td>$membership[membershipname]</td>
                </tr>
                <tr>
                    <td>IC: </td>
                    <td>$membership[ic]</td>
                </tr>
                <tr>
                    <td>Phone: </td>
                    <td>$membership[phone]</td>
                </tr>
                <tr>
                    <td>Address: </td>
                    <td>$membership[address]</td>
                </tr>
                <tr>
                    <td>Email: </td>
                    <td>$membership[email]</td>
                </tr>
                <tr>
                    <td>Branch: </td>
                    <td>$membership[branch]</td>
                </tr>
                
                <tr>
                    <td>Time: </td>
                    <td>$membership[applytime]</td>
                </tr>
		    </table><br>";
}
echo $str;
?>
			</div>
	</div>
	<div class="footer"><?php include 'footer.php';?></div>
	</div>
</body>
</html>