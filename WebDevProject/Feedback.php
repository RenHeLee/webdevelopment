<!-- Feedback page for client -->


<?php
$enquiry_message = '';
if (! empty($_POST['FeedbackForm'])) {
    require 'database.php';
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $title = trim($_POST['title']);
    $branch = trim($_POST['branch']);
    $feedback = trim($_POST['feedback']);

    client::insertFeedback($name, $email, $title, $branch, $feedback); // check user login
    $enquiry_message = 'Feedback is sent successfully.';
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style2.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<title>FEEDBACK</title>
</head>

<body>

	<div class="topnav">
		<a href="index.php">Home</a> <a href="Membership.php">Membership</a>
		<a href="Promotion.php">Promotion</a> <a class="active" href="Feedback.php">Feedback</a>
		<a href="About.php">About</a>
	</div>
	<div class="header">
		<img src="logo.png" alt="logo" width="371" height="257">
		<h2>
			<b>S I N C E &nbsp 1 9 9 3</b>
		</h2>
	</div>

	<div style="padding-left: 10%; padding-right: 10%; text-align: center;">
	<?php
if ($enquiry_message != "") {
    echo '<div><strong>Message: </strong> ' . $enquiry_message;
}
?>
	<form action="Feedback.php" method="post">
			<h2>Feedback Form</h2>
			<p>Name:</p>
			<input type="text" id="name" name="name" style="width: 50%" required><br>
			<p>Email:</p>
			<input type="email" id="email" name="email" style="width: 50%"
				required><br>
			<p>Title:</p>
			<input type="text" id="title" name="title" style="width: 50%"
				required><br>
			<p>Branch:</p>
			<select id="branch" name="branch" required style="width: 50%">
				<option value="">Please select branch</option>
				<option value="Arked">Arked</option>
				<option value="Bestari">Bestari</option>
				<option value="Perwira">Perwira</option>
				<br>
			</select><br>
			<p>Feedback:</p>
			<textarea rows="8" cols="100" id="feedback" name="feedback"
				style="width: 50%"></textarea><br>
			<input type="submit" value="Submit" name="FeedbackForm"> <input type="submit"
				value="Reset"><br>
		</form>
	</div>

<div class="footer">
		<?php include 'footer.php';?>
	</div>
</body>
</html>