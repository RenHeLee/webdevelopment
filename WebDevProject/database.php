<?php

try {
    $pdo = new PDO("mysql:host = 'localhost'; dbname = 'web_project'", 'root', '');
    // set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "";
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}


class User {
    //check the multidimentional array
    function isMultidimensional($array)
    {
        return count($array) !== count($array, COUNT_RECURSIVE);
    }
    
    //Select contact
    function selectContact()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT * FROM web_project.contact;");
            $query->execute();
            if ($query->rowCount() > 0) {
                return $query->fetchAll();
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
}

class Client extends User{
    
    
    //Insert membership request
    function insertMembership($membershipname, $email, $phone, $ic, $branch, $address)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("INSERT INTO web_project.membership (membershipname, email, phone, ic, branch, address, applytime) VALUE(:membershipname, :email, :phone, :ic, :branch, :address, :applytime);");
            $query->bindParam(":membershipname", $membershipname, PDO::PARAM_STR);
            $query->bindParam(":email", $email, PDO::PARAM_STR);
            $query->bindParam(":phone", $phone, PDO::PARAM_STR);
            $query->bindParam(":ic", $ic, PDO::PARAM_STR);
            $query->bindParam(":branch", $branch, PDO::PARAM_STR);
            $query->bindParam(":address", $address, PDO::PARAM_STR);
            $time = date("Y-m-d H:i:s");
            $query->bindParam(":applytime", $time, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    //Insert feedback
    function insertFeedback($enquiryname, $email, $title, $branch, $feedback)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("INSERT INTO web_project.feedback (enquiryname, email, title, branch, feedback, feedbacktime) VALUE(:enquiryname, :email, :title, :branch, :feedback, :feedbacktime);");
            $query->bindParam(":enquiryname", $enquiryname, PDO::PARAM_STR);
            $query->bindParam(":email", $email, PDO::PARAM_STR);
            $query->bindParam(":title", $title, PDO::PARAM_STR);
            $query->bindParam(":branch", $branch, PDO::PARAM_STR);
            $query->bindParam(":feedback", $feedback, PDO::PARAM_STR);
            $time = date("Y-m-d H:i:s");
            $query->bindParam(":feedbacktime", $time, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
}


class Admin extends User{
    /*
     * Login
     *
     * @param $userInput, $password
     * @return $mixed
     */
    function login($userInput, $password)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT user_id FROM web_project.user WHERE (username=:userInput) AND hashedpassword=:hashedPassword");
            $query->bindParam(":userInput", $userInput, PDO::PARAM_STR);
            $hashedPassword = hash('sha256', $password);
            $query->bindParam(":hashedPassword", $hashedPassword, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                $result = $query->fetch(PDO::FETCH_OBJ);
                return $result->user_id;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    //Insert contact
    function insertContact($name, $email, $phone)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("INSERT INTO web_project.contact (contact_name, email, phone) VALUE(:name, :email, :phone);");
            $query->bindParam(":name", $name, PDO::PARAM_STR);
            $query->bindParam(":email", $email, PDO::PARAM_STR);
            $query->bindParam(":phone", $phone, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    
    
    //Delete contact
    function deleteContact($id)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("DELETE FROM web_project.contact WHERE id = :id;");
            $query->bindParam(":id", $id, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    
    
    //Select feedback
    function selectFeedback()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT * FROM web_project.feedback ORDER BY feedbacktime DESC;");
            $query->execute();
            if ($query->rowCount() > 0) {
                return $query->fetchAll();
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    
    
    //Select membership request
    function selectMembership()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT * FROM web_project.membership ORDER BY applytime DESC;");
            $query->execute();
            if ($query->rowCount() > 0) {
                return $query->fetchAll();
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
}





?>