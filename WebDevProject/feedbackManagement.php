<!-- Management page for feedback -->
<?php
// Start Session
session_start();

// check user login
if (empty($_SESSION['user_id'])) {
    header("Location: login.php");
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="style2.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<title>Feedback Management</title>
</head>
<body>
	<div class="topnav">
		<a href="index.php">Home</a> <a
			href="membershipManagement.php">Membership Management</a> <a
			class="active" href="feedbackManagement.php">Feedback Management</a> <a
			href="contactManagement.php">Contact Management</a>
	</div>
	<div class="content">
		<h2>Feedback</h2>
			<?php
require 'database.php';
$feedback = Admin::selectFeedback();

$str = '';
if (Admin::isMultidimensional($feedback)) {
    foreach ($feedback as $key) {
        $str .= "<table>
                        <tr>
                            <td>Name: </td>
                            <td>$key[enquiryname]</td>
                        </tr>
                        <tr>
                            <td>Email: </td>
                            <td>$key[email]</td>
                        </tr>
                        <tr>
                            <td>Title: </td>
                            <td>$key[title]</td>
                        </tr>
                        <tr>
                            <td>Branch: </td>
                            <td>$key[branch]</td>
                        </tr>
                        <tr>
                            <td>Feedback: </td>
                            <td>$key[feedback]</td>
                        </tr>
                        <tr>
                            <td>Time: </td>
                            <td>$key[feedbacktime]</td>
                        </tr>
				</table><br>";
    }
    ;
} else {
    $str .= "<table>
                        <tr>
                            <td>Name: </td>
                            <td>$feedback[enquiryname]</td>
                        </tr>
                        <tr>
                            <td>Email: </td>
                            <td>$feedback[email]</td>
                        </tr>
                        <tr>
                            <td>Title: </td>
                            <td>$feedback[title]</td>
                        </tr>
                        <tr>
                            <td>Branch: </td>
                            <td>$feedback[branch]</td>
                        </tr>
                        <tr>
                            <td>Feedback: </td>
                            <td>$feedback[feedback]</td>
                        </tr>
                        <tr>
                            <td>Time: </td>
                            <td>$feedback[feedbacktime]</td>
                        </tr>
				</table><br>";
    ;
}
echo $str;
?>
			</div>
	</div>
	<div class="footer"><?php include 'footer.php';?></div>
	</div>
</body>
</html>